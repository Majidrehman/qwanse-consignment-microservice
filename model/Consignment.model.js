'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const consignmentSchema = new Schema({
  markNumber: String,
  consignor: {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    name: String,
    contactPerson: String,
    country: String,
    city: String,
    state: String,
    location: String,
    postalCode: String,
    longitude: Number,
    latitude: Number,
    phone: String,
    email: String
  },
  consignee: {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    name: String,
    contactPerson: String,
    country: String,
    city: String,
    state: String,
    location: String,
    postalCode: String,
    longitude: Number,
    latitude: Number,
    phone: String,
    email: String
  },
  carrier: {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    name: String,
    contactPerson: String,
    country: String,
    city: String,
    state: String,
    location: String,
    postalCode: String,
    longitude: Number,
    latitude: Number,
    phone: String,
    email: String
  },
  pickUp: {
    country: String,
    city: String,
    state: String,
    location: String,
    postalCode: String,
    street: String,
    longitude: Number,
    latitude: Number,
    contactPerson: String,
    phone: String,
    fax: String,
    openingTime: String,
    closingTime: String
  },
  delivery: {
    country: String,
    city: String,
    state: String,
    location: String,
    postalCode: String,
    street: String,
    longitude: Number,
    latitude: Number,
    contactPerson: String,
    phone: String,
    fax: String,
    openingTime: String,
    closingTime: String
  },
  createdBy: {
    id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    role: {
      type: String,
      enum: ['Consignor', 'Carrier']
    }
  },
  pickUpDate: Date,
  deliveryDate: Date,
  totalDistance: Number,
  agreement: String,
  terms: String,
  contract: {
    isContract: {
      type: Boolean,
      default: false
    },
    contractDate: Date
  },
  packages: [{
    serialNumber: String,
    brand: String,
    methodOfPackaging: String,
    grossWeight: Number,
    grossWeightUnit: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Unit'
    },
    volume: Number,
    volumeUnit: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Unit'
    },
    nature: String,
    value: Number,
    perishable: Boolean,
    minTemperature: Number,
    maxTemperature: Number,
    packageCategory: {
      label: String,
      guidance: String,
      characteristics: String,
      class: {
        type: String,
        minlength: 1,
        maxlength: 3
      }
    },
    container: {
      registration: String,
      owner: String,
      latitude: Number,
      longitude: Number,
      containerType: {
        type: {
          type: String
        },
        length: Number,
        width: Number,
        height: Number,
        weightFree: Number,
        weightPayload: Number,
        unit: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Unit'
        }
      }
    },
    isDeleted: {
      type: Boolean,
      default: false
    }
  }],
  isPackagesVerified: { // will set to true when driver verifies packages against consignment
    type: Boolean,
    default: false
  },
  statusesHistory: [{
    title: {
      type: String,
      enum: ['Draft', 'Creating', 'AwaitingApproval', 'AwaitingQuotation', 'Contract', 'Ready','DriverAtLocation', 'VerifyPackages', 'Loading', 'SenderSign', 'InTransition', 'DriverAtDestination', 'Unloading', 'ReceiverSign', 'Delivered', 'Completed', 'Canceled']
    },
    time: {
      type: Date,
      default: Date.now
    }
  }],
  currentStatus: {
    type: String,
    enum: ['Draft', 'Creating', 'AwaitingApproval', 'AwaitingQuotation', 'Contract', 'Ready','DriverAtLocation', 'VerifyPackages', 'Loading', 'SenderSign', 'InTransition', 'DriverAtDestination', 'Unloading', 'ReceiverSign', 'Delivered', 'Completed', 'Canceled']
  },
  chargesToBePaidBy: {
    base: {
      type: String,
      enum: ['Consignor', 'Consignee'],
      default: 'Consignor'
    },
    supplementary: {
      type: String,
      enum: ['Consignor', 'Consignee'],
      default: 'Consignor'
    },
    taxAndDuty: {
      type: String,
      enum: ['Consignor', 'Consignee'],
      default: 'Consignor'
    },
    other: {
      type: String,
      enum: ['Consignor', 'Consignee'],
      default: 'Consignor'
    }
  },
  isInvoiced: {
    type: Boolean,
    default: false
  },
  consignorSign: String,
  consigneeSign: String,
  carrierSign: String,
  splits: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Split'
  }],
  quotationCarriers: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }]
},
  {
    timestamps: { createdAt: true, updatedAt: false }
  })

const Consignment = mongoose.model('Consignment', consignmentSchema)

module.exports = { Consignment }
