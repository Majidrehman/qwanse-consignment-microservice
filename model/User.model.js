const mongoose = require('mongoose')
let crypto = require('crypto')

const Schema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    index: true,
    minlength: 3,
    maxlength: 100,
    lowercase: true // Always convert `email` to lowercase
  },
  companyName: {
    type: String,
    minlength: 3,
    maxlength: 100
  },
  contactNumber: {
    type: String,
    minlength: 8,
    maxlength: 18
  },
  street: {
    type: String,
    maxlength: 100
  },
  CountryId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Country'
  },
  CityId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'City'
  },
  currency: {
    type: String
  },
  language: {
    type: String
  },
  isVerified: { // to check whether the user has verified the email or not
    type: Boolean,
    default: false
  },
  isActive: { // to check whether the user is blocked by super admin or not
    type: Boolean,
    default: true
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  RoleId: { // Role may be Carrier, Consignor, Employee
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Role'
    // required: true
  },
  EmployeeCompanyId: { // In case if user is employee his reference company.
    type: mongoose.Schema.Types.ObjectId
    // ref: 'User'
  },
  accountingEmail: {
    type: String,
    minlength: 3,
    maxlength: 100,
    lowercase: true // Always convert `email` to lowercase
  }
},
{
  timestamps: { createdAt: true, updatedAt: false }
})

const User = mongoose.model('User', Schema)

module.exports = {
  User
}
