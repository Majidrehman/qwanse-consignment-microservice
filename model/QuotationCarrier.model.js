const mongoose = require('mongoose')

const Schema = new mongoose.Schema({
  CarrierId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  ConsignmentId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Consignment',
    required: true
  },
  name: String,
  contactPerson: String,
  phone: String,
  email: String,
  consignorApproval: {
    type: String,
    enum: ['Pending', 'Approved', 'Rejected'],
    default: 'Pending'
  },
  carrierApproval: {
    type: String,
    enum: ['Pending', 'Approved', 'Rejected'],
    default: 'Pending'
  },
  proposedBy: {
    type: String,
    enum: ['Consignor', 'Carrier']
  },
  isWithdrawn: {
    type: Boolean,
    default: false
  },
  notes: String,
  quotation: [{
    type: {
      type: String,
      enum: ['Base', 'Supplementary', 'TaxAndDuty', 'Other']
    },
    amount: Number,
    currency: String,
    dueDate: Date
  }],
  isDeleted: {
    type: Boolean,
    default: false
  }
},
{
  timestamps: { createdAt: true, updatedAt: false }
})

const QuotationCarrier = mongoose.model('QuotationCarrier', Schema)

module.exports = { QuotationCarrier }
