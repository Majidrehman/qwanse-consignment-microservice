'use strict'
const mongoose = require('mongoose')

const Schema = new mongoose.Schema({
  consignorId: { // id of sender company
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  carrierId: { // id of carrier company
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  consignee: { // id of receiver company
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  isDeleted: {
    type: Boolean,
    default: false
  }
},
{
  timestamps: { createdAt: true, updatedAt: false }
})

const Handshake = mongoose.model('Handshake', Schema)

module.exports = {
  Handshake
}
