
# CONSIGNMENT MICRO SERVICE
---
###OVERVIEW



Consignment service handles the consignment related tasks like create, update, draft and delete
consignment. Consignment creator can make new handshake to affiliate other companies as a
carrier or a sender. If user does not exist when making handshake, new user created and send
the new user data in the Auth Micro service SQS.
When a message arrives in the SQS queue, it validates its data and saves it to the database with
a provided valid identifier.
When a new consignment created or edit or deleted, it sends the message along with the data to the Auth
Micro service SQS and ping the notification & activity micro service to push the related notifications.

---
###INPUT & OUTPUT DATA 
####Input Data from Auth Microservice SQS
```css
 {
 userId
• companyName
• phone
• email
• CountryId
• CityId
• Currency
• language
• isVerified
• isActive
• isDeleted
• RoleId
• EmployeeComapnyId
• accountingEmail
• addresses
• vehicles
}
```


####Output Data SQS

#####To Auth, Dispute and Claim microservices
```
 {
userData,
consignmentData{},
Consignment,
Status

}
```

#####To Notification & Activity microservice
```
 {
UserId: {
   type: mongoose.Schema.Types.ObjectId
 },
 identifier: String,
 data: {}

Notification output data.
UserId: {
   type: mongoose.Schema.Types.ObjectId
 },
 EmployerId: {
   type: mongoose.Schema.Types.ObjectId
 },
 ConsignmentId: {
   type: mongoose.Schema.Types.ObjectId
 },
 SplitId: {
   type: mongoose.Schema.Types.ObjectId
 },
 identifier: String,
 data: {}
Activity output data.

executionTime: {
   type: Date
 },
 replyQueue: String,
 data: {}
Scheduler Service Output data.
}
```

###SEQUENCE DIAGRAMS

####Create Consignment
![Alt text](/sequence-diagram/consignment-create.png)

####Update Consignment 
![Alt text](/sequence-diagram/consignment-update.png)

####Add Routes
![Alt text](/sequence-diagram/routes-add.png)

####Update Routes
![Alt text](/sequence-diagram/routes-update.png)

####Delete Routes
![Alt text](/sequence-diagram/routes-delete.png) 

####Outsource Routes
![Alt text](/sequence-diagram/resources-outsourced.png)


####Assign Resources
![Alt text](/sequence-diagram/resources-assign.png)

####Add Offer 
![Alt text](/sequence-diagram/offer-add.png)

####Accept Offer
![Alt text](/sequence-diagram/offer-accept.png)

####Draft Offer
![Alt text](/sequence-diagram/offer-draft.png)


###DATA MODEL

####Consignment
```
{
 markNumber: String,
 consignor: {
 id: {
 type: mongoose.Schema.Types.ObjectId,
 ref: 'User'
 },
 name: String,
 contactPerson: String,
 country: String,
 city: String,
 state: String,
 location: String,
 postalCode: String,
 longitude: Number,
 latitude: Number,
 phone: String,
 email: String
 },
 consignee: {
 id: {
 type: mongoose.Schema.Types.ObjectId,
 ref: 'User'
 },
 name: String,
 contactPerson: String,
 country: String,
 city: String,
 state: String,
 location: String,
 postalCode: String,
 longitude: Number,
 latitude: Number,
 phone: String,
 email: String
 },
 carrier: {
 id: {
 type: mongoose.Schema.Types.ObjectId,
 ref: 'User'
 },
 name: String,
 contactPerson: String,
 country: String,
 city: String,
 state: String,
 location: String,
 postalCode: String,
 longitude: Number,
 latitude: Number,
 phone: String,
 email: String
 },
 pickUp: {
 country: String,
 city: String,
 state: String,
 location: String,
 postalCode: String,
 street: String,
 longitude: Number,
 latitude: Number,
 contactPerson: String,
 phone: String,
 fax: String,
 openingTime: String,
 closingTime: String
 },
 delivery: {
 country: String,
 city: String,
 state: String,
 location: String,
 postalCode: String,
 street: String,
 longitude: Number,
 latitude: Number,
 contactPerson: String,
 phone: String,
 fax: String,
 openingTime: String,
 closingTime: String
 },
 createdBy: {
 id: {
 type: mongoose.Schema.Types.ObjectId,
 ref: 'User'
 },
 role: {
 type: String,
 enum: ['Consignor', 'Carrier'
]

}
 },
 pickUpDate: Date,
 deliveryDate: Date,
 totalDistance: Number,
 agreement: String,
 terms: String,
 isContract: {
 type: Boolean,
 default: false
 },
 contractDate: Date,
 packages: [{
 serialNumber: String,
 brand: String,
 methodOfPackaging: String,
 grossWeight: Number
,
 grossWeightUnit: {
 type: mongoose.Schema.Types.ObjectId,
 ref: 'Unit'
 },
 volume: Number,
 volumeUnit: {
 type: mongoose.Schema.Types.ObjectId,
 ref: 'Unit'
 },
 nature: String,
 value: Number,
 perishable: Boolean,
 minTemperature: Number,
 maxTemperature: Number,
 packageCategory: {
 label: String,
 guidance: String,
 characteristics: String,
 class: {
 type: String,
 minlength: 1,
 maxlength: 3
 }
 },
 container: {
 registration: String,
 owner: String,
 latitude: Number,
 longitude: Number,
 containerType: {
 type: {
 type: String
 },
 length: Number,
 width: Number,
 height: Number,
 weightFree: Number,
 weightPayload: Number,
 unit: {
 type: mongoose.Schema.Types.ObjectId,
 ref: 'Unit'
 }
 }
 },
 isDeleted: {
 type: Boolean,
 default: false
 }
 }],
 isPackagesVerified: { // will set to true when driver verifies packages against consignment
 type: Boolean,
 default: false
 },
 statusesHistory: [{
 title: {
 type: String,
 enum: ['Draft', 'Creating', 'AwaitingApproval', 'AwaitingQuotation', 'Contract',
'Ready', 'PickUp', 'InTransition', 'Delivered', 'Completed']
 },
 time: {
 type: Date,
 default: Date.now

 }
 }],
 currentStatus: {
 type: String,
 enum: ['Draft', 'Creating', 'AwaitingApproval', 'AwaitingQuotation', 'Contract', 'Ready',
'PickUp', 'InTransition', 'Delivered', 'Completed']
 },
 chargesToBePaidBy: {
 base: {
 type: String,
 enum: ['Consignor', 'Consignee'],
 default: 'Consignor'
 },
 supplementary: {
 type: String,
 enum: ['Consignor', 'Consignee'],
 default: 'Consignor'
 },
 taxAndDuty: {
 type: String,
 enum: ['Consignor', 'Consignee'],
 default: 'Consignor'
 },
 other: {
 type: String,
 enum: ['Consignor', 'Consignee'],
 default: 'Consignor'
 }
 },
 isInvoiced: {
 type: Boolean,
 default: false
 },
 consignorSign: String,
 consigneeSign: String,
 carrierSign: String,
 splits: [{
 type: mongoose.Schema.Types.ObjectId,
 ref: 'Split'
 }],
 quotationCarriers: [{
 type: mongoose.Schema.Types.ObjectId,
 ref: 'User'
 }]
},
{
 timestamps: { createdAt: true, updatedAt: false }

}
```

####Split 

```
{
 startPoint: {
 country: String,
 city: String,
 state: String,
 location: String,
 postalCode: String,
 street: String,
 longitude: Number,
 latitude: Number,
 contactPerson: String,
 phone: String,
 fax: String,
 openingTime: String,
 closingTime: String,
 qrCode: String // QR code generated by contact person for sign before driver depart from
start point
 },
 endPoint: {
 country: String,
 city: String,
 state: String,
 location: String,
 postalCode: String,
 street: String,
 longitude: Number,
 latitude: Number,
 contactPerson: String,
 phone: String,
 fax: String,
 openingTime: String,
 closingTime: String,
 qrCode: String // QR code generated by receiver for sign after driver reached at endpoint
 },
 pickUpDate: Date,
 deliveryDate: Date,
 isOutsourced: {
 type: Boolean,
 default: false
 },
 OutsourcedConsignmentId: {
 type: mongoose.Schema.Types.ObjectId,
 ref: 'Consignment'
 },
 VehicleId: {
 type: mongoose.Schema.Types.ObjectId,
 ref: 'Vehicle',
 default: null
 },
 DriverId: {
 type: mongoose.Schema.Types.ObjectId,
 ref: 'User',
 default: null
 },
 // verification pins
 driverVerificationPin: {
 type: String
 },
 pickupVerificationPin: {
 type: String
 },
 dropOffVerificationPin: {
 type: String
 },
 statuses: [{
 title: {
 type: String,
 enum: ['Created', 'Ready', 'DriverAtLocation', 'VerifyPackages', 'Loading',
'SenderSign', 'InTransition', 'Unloading', 'ReceiverSign', 'Completed']
 },
 time: {
 type: Date,
 default: Date.now
 }
 }],
 currentStatus: {
 type: String,
 enum: ['Created', 'Ready', 'DriverAtLocation', 'VerifyPackages', 'Loading', 'SenderSign',
'InTransition', 'Unloading', 'ReceiverSign', 'Completed'],
 default: 'Created'
 },
 isDeleted: {
 type: Boolean,
 default: false
 },
 ConsignmentId: {
 type: mongoose.Schema.Types.ObjectId,
 ref: 'Consignment'
,
 required: true

}
},{
 timestamps: { createdAt: true, updatedAt: false
}
}
```


####Quotation Carrier

```
{
 CarrierId: {
 type: mongoose.Schema.Types.ObjectId,
 ref: 'User'
,
 required: true
 },
 ConsignmentId: {
 type: mongoose.Schema.Types.ObjectId,
 ref: 'Consignment'
,
 required: true
 },
 name: String,
 contactPerson: String,
 phone: String,
 email: String,
 consignorApproval: {
 type: String,
 enum: ['Pending', 'Approved', 'Rejected'],
 default: 'Pending'
 },
 carrierApproval: {
 type: String,
 enum: ['Pending', 'Approved', 'Rejected'],
 default: 'Pending'
 },
 proposedBy: {
 type: String,
 enum: ['Consignor', 'Carrier'
]
 },
 isWithdrawn: {
 type: Boolean,
 default: false
 },
 notes: String,
 quotation: [{
 type: {
 type: String,
 enum: ['Base', 'Supplementary', 'TaxAndDuty', 'Other']
 },
 amount: Number,
 currency: String,
 dueDate: Date
 }],
 isDeleted: {
 type: Boolean,
 default: false
 }
},
{
 timestamps: { createdAt: true, updatedAt: false }
}
```


###SWAGGER

![Alt text](swagger/userSwagger.PNG)

![Alt text](swagger/roleSwagger.PNG)

![Alt text](swagger/vehicleTypeSwagger.PNG)

![Alt text](swagger/vehicleswagger.PNG)

![Alt text](swagger/employeeSwagger.PNG)

![Alt text](swagger/jobSwagger.PNG)

![Alt text](swagger/unitSwagger.PNG)

![Alt text](swagger/countrySwagger.PNG)

![Alt text](swagger/citySwagger.PNG)

![Alt text](swagger/stateSwagger.PNG)

